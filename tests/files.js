/**
 * Unit tests for the files route.
 * @module
 * @requires tests/server
 */

var assert  = require('chai').assert,
    async   = require('async'),
    fs      = require('fs'),
    path    = require('path'),
    request = require('supertest'),
    uuid    = require('uuid');

var server = require('./server');

describe('Testing the files route.', function () {

  var currentServer;
  var testResourceFile = path.join(__dirname, 'resources/file.txt');

  var parseFile = function (res, callback) {
    res.data = [];
    res.on('data', function (chunk) {
      res.data.push(chunk);
    });
    res.on('end', function () {
      callback(null, Buffer.concat(res.data));
    });
  };

  beforeEach(function () {
    currentServer = server(3000);
  });

  afterEach(function (done) {
    currentServer.close(done);
  });

  it('Upload a file and get a valid identifier back', function testUpload(done) {
    request(currentServer)
        .post('/files/')
        .attach('file', testResourceFile)
        .expect(200)
        .end(function (err, res) {
          assert.ifError(err);
          assert.ok(/^[0-9a-f]{32}$/.test(res.text)); // Check correct format.
          done();
        });
  });

  it('Upload two files, and verify different IDs', function testUpload(done) {
    async.waterfall([
      function (cb) {
        request(currentServer)
            .post('/files/')
            .attach('file', testResourceFile)
            .expect(200)
            .end(function (err, res) {
              assert.ifError(err);
              cb(null, res.text);
            });
      }, function (firstId, cb) {
        request(currentServer)
            .post('/files/')
            .attach('file', testResourceFile)
            .expect(200)
            .end(function (err, res) {
              assert.ifError(err);
              assert.notEqual(res.text, firstId);
              cb();
            });
      }
    ], done);
  });


  it('Upload and retrieve a file', function testDownload(done) {
    async.waterfall([
      function (cb) {
        request(currentServer)
            .post('/files/')
            .attach('file', testResourceFile)
            .expect(200)
            .end(function (err, res) {
              assert.ifError(err);
              cb(null, res.text);
            });
      }, function (firstId, cb) {
        request(currentServer)
            .get('/files/' + firstId)
            .parse(parseFile)
            .expect(200)
            .end(function (err, res) {
              assert.ifError(err);

              assert.ok(Buffer.isBuffer(res.body));
              assert.deepEqual(res.body, fs.readFileSync(testResourceFile));
              cb();
            });
      }
    ], done);
  });

  it('Check that incorrect files are not retrieved', function testBadNames(done) {
    var getId = function () { return uuid.v4().replace(/-/g, ''); };

    var tests = [];

    // Add tests for the incorrect lengths.
    var getIncorrectLengthTest = function (length) {

      // Add incorrect length tests.
      if (length < 1 || length == 32 || length > 33) {
        return function (cb) { cb(new Error('Incorrect length given to testIncorrectLength')); };
      } else if (length < 32) {
        return function (cb) {
          request(currentServer)
              .get('/files/' + getId().substring(0, length))
              .expect(400, cb);
        };
      } else {
        return function (cb) {
          request(currentServer)
              .get('/files/' + getId() + 'a')
              .expect(400, cb);
        };
      }
    };

    for (var i = 1; i < 32; i++) {
      tests.push(getIncorrectLengthTest(i));
    }
    tests.push(getIncorrectLengthTest(33));

    // Add a test for no parameter.
    tests.push(function (cb) {
      request(currentServer)
          .get('/files/')
          .expect(404, cb);
    });

    // Add a test for a bad id.
    tests.push(function (cb) {
      request(currentServer)
          .get('/files/' + 'g' + getId().substring(1))
          .expect(400, cb);
    });

    // Add a test for a non existent file.
    tests.push(function (cb) {
      request(currentServer)
          .get('/files/' + getId())
          .expect(404, cb);
    });

    async.series(tests, done);
  });

});