/**
 * Server application.
 * @module
 * @requires config
 */

var bodyParser   = require('body-parser'),
    cookieParser = require('cookie-parser'),
    express      = require('express'),
    favicon      = require('serve-favicon'),
    logger       = require('morgan'),
    path         = require('path');

var config = require('./config');

var app = express();

// App settings.
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(logger(config.logger.format, config.logger.options));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());


// Setup routes.
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', require('./routes/index'));
app.use('/files', require('./routes/files'));

// Send a 404 for remaining routes.
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// Error handlers.
if (app.get('env') === 'development') {
  // Development handler.
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error:   err
    });
  });
} else {
  // Production handler.
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error:   {}
    });
  });
}

module.exports = app;
