/**
 * Configuration module.
 * @module
 */

var _    = require('lodash'),
    fs   = require('fs'),
    path = require('path');

/**
 * The configuration data.
 */
var config = {
  /**
   * Settings for the server.
   * @type {Object}
   * @prop int defaultPort Default port to start the server on if none given in the command line.
   */
  server: {
    defaultPort: 3001
  },

  /**
   * Settings for the logger.
   * @type {Object}
   * @prop {string} format Format of the logger.
   * @prop {string} options Options to pass to the logger.
   */
  logger: {
    format:  'dev',
    options: {}
  },

  /**
   * Settings for the files route.
   * @type {Ojbect}
   * @prop {string} uploadPath Path to store uploads at.
   */
  files: {
    uploadPath: path.join(__dirname, 'public/uploads')
  }
};


// If deploy-config exists, merge its settings with these settings.
var deployConfigAvailable = false;
try {
  fs.accessSync(path.join(__dirname, './deploy-config.js'), fs.F_OK);
  deployConfigAvailable = true;
} catch (e) { }

if (deployConfigAvailable) {
  _.merge(config, require('./deploy-config'));
}

module.exports = config;