/**
 * Route handler for the index page.
 * @module
 */

var express = require('express');

var router = express.Router();

// GET home page.
router.get('/', function(req, res, next) {
  res.render('index', { title: 'MessageGuard File Server' });
});

module.exports = router;
