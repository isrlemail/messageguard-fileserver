/**
 * Route handler for the files route.
 * @module
 * @requires config
 */

var express = require('express'),
    fs      = require('fs'),
    multer  = require('multer'),
    path    = require('path');

var config = require('../config');

var router = express.Router();

// POST stores file. Returns a UUID for the file.
var fileSizeLimit = 10 * 1024 * 1024; // 10 MB.
var multerSettings = {
  storage: multer.diskStorage({destination: config.files.uploadPath}),
  limits:  {fileSize: fileSizeLimit, files: 1}
};

router.post('/', multer(multerSettings).single('file'), function (req, res, next) {
  res.send(req.file.filename);
});

// GET grabs the requested file.
var filenameRegex = /^[0-9,a-f]{32}$/

router.get('/:fileId', function (req, res, next) {
  if (filenameRegex.test(req.params.fileId)) {
    var fileName = path.join(config.files.uploadPath, req.params.fileId);
    fs.stat(fileName, function (err, stats) {
      if (!err && stats.isFile()) {
        res.sendFile(fileName);
      } else {
        // Bad request. This will go to the not found error handler.
        next();
      }
    });
  } else {
    res.status(400).send('Bad file ID');
  }
});

module.exports = router;
