#!/usr/bin/env node

/**
 * Runs the tests for the key server.
 */

var _     = require('lodash'),
    glob  = require('glob'),
    Mocha = require('mocha'),
    path  = require('path');

var mocha = new Mocha();
var testFiles = path.join(__dirname, '../tests', '**/*.js');

// Change the config for testing.
_.merge(require('../config'), {
  logger: {
    options: {
      skip: function () { return true; }
    }
  }
});

// Add the tests.
glob.sync(testFiles).forEach(function (file) {
  mocha.addFile(file);
});

// Run the tests.
mocha.run(function (failures) {
  process.on('exit', function () { process.exit(failures); });
});
